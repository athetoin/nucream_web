/**
 * 
 */
const header = document.querySelector('header');
const firstSection = document.querySelectorAll('section')[0];
const mark = document.getElementById('mark');
const prdtMnu = document.getElementById('prdt-mnu');
const nav = document.querySelector('nav');
const svgNs = 'http://www.w3.org/2000/svg';

var drpIcn;

function setTopMargin() {
    firstSection.style.marginTop = header.offsetHeight + 'px';
}

function onWindowsResize() {
    setTopMargin();
}

function createNavBtn() {
    const btn0 = document.createElementNS(svgNs, 'svg');
    btn0.id = "nav-btn";
    const use = document.createElementNS(svgNs, 'use');
    use.setAttribute('href', 'image/menu.svg#menu-smbl');
    btn0.appendChild(use);
    nav.appendChild(btn0);
}

function createPrdtBtn() {
    var lbl = prdtMnu.innerText;
    const children = prdtMnu.childNodes;
    for (let i = 0; i < children.length; i++) {
        if (children[i].nodeName === '#text') {
            children[i].nodeValue = '';
        }
    }

    const ttl = document.createElement('div');
    ttl.id = 'prdt-ttl';
    ttl.classList.add('cur-nav');
    ttl.appendChild(document.createTextNode(lbl));

    drpIcn = document.createElementNS(svgNs, 'svg');
    drpIcn.id = 'prdt-icn'
    const use = document.createElementNS(svgNs, 'use');
    use.setAttribute('href', 'image/menu.svg#menu-drop');

    drpIcn.appendChild(use);
    ttl.appendChild(drpIcn);
    prdtMnu.appendChild(ttl);
    ttl.onclick = dropMenu;
}

function markLink() {
    window.location.href = 'index.html';
}

function dropMenu() {
    const ulElmt = prdtMnu.querySelector('ul');

    if (drpIcn.style.transform == '') {
        drpIcn.style.transform = 'rotate(180deg)'
        ulElmt.style.display = 'flex';
    } else {
        drpIcn.style.transform = '';
        ulElmt.style.display = '';
    }
}

createNavBtn();
createPrdtBtn();
onWindowsResize();
mark.onclick = markLink;
window.onresize = onWindowsResize;